﻿namespace ListViewSample
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class Person : INotifyPropertyChanged
    {
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        public string Name { get; }

        public int Age { get; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}